const form = document.getElementById("todo-form");
const todoInput = document.getElementById("todo");
const todoList = document.querySelector(".list-group");
const firstCardBody = document.querySelectorAll(".card-body")[0];
const secondCardBody = document.querySelectorAll(".card-body")[1];
const filter = document.querySelector("#filter");
const clearButton = document.querySelector("#clear-todos");
{/* <li class="list-group-item d-flex justify-content-between">
                            Todo 1
                            <a href = "#" class ="delete-item">
                                <i class = "fa fa-remove"></i>
                            </a>

                        </li> */}
function eventListeners() {
    form.addEventListener("submit",addTodo);
    clearButton.addEventListener("click",clearTodos);
    function getTodosLocalStorage(){
        let todos;
        if(localStorage.getItem("todos")===null){
            todos = [];
        }else{
            todos = JSON.parse(localStorage.getItem("todos"));
        }
        return todos;
    }
    function setTodosLocalStorage(newTodoItem) {
        let todos  = getTodosLocalStorage();
        console.log(todos);
        todos.push(newTodoItem);
        localStorage.setItem("todos",JSON.stringify(todos));
    }
    
 
   
    function addTodo(e) {
    
        e.preventDefault();
        const newTodo = document.createElement("li");
        const newTodoText = document.createTextNode(todoInput.value);
        const newTodoChild = document.createElement("a");
        const newTodoChildContent = document.createElement("i");
        newTodoChildContent.className="fa fa-remove";
        newTodo.className="list-group-item d-flex justify-content-between";
        newTodo.appendChild(newTodoText);
        newTodo.appendChild(newTodoChild);
        newTodoChild.href="#";
        newTodoChild.className="delete-item";
        setTodosLocalStorage(newTodoText.textContent);
        newTodoChild.appendChild(newTodoChildContent);
        todoList.appendChild(newTodo);
        
        console.log(newTodo);
    }
    function clearTodos(e) {
        e.preventDefault();
       
        while(todoList.firstChild !== null){
            todoList.firstChild.remove();   
        }

    
    }
}
eventListeners();